﻿#include <iostream>

class Animal
{
public:
    virtual ~Animal(){}
   virtual void makeSound()
    {
        std::cout << "makeSound()" << std::endl;
    }
   
 
};

class Cow : public Animal
{
public:
    void makeSound()
    {
        std::cout << "Muu!\n";
    }
};

class Dog : public Animal
{
public:
    void makeSound()
    {
        std::cout << "Woof!\n";
    }
};

int main()
{
    Animal* animals[2];
    animals[0] = new Cow();
    animals[1] = new Dog();

    for (Animal* a : animals)
        a->makeSound();
    delete animals[0]; delete animals[1];
   
} 


